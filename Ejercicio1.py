import sys

def duck_duck_goose(lista_bruta):
    lista_bruta.pop(0)
    try:
        numero = int(lista_bruta.pop(0))
    except:
        return print("No se ha introducido un numero")
    if not numero > 0:
        return print("Numero introducido no valido")
    lista_jugadores = lista_bruta 
    if lista_jugadores == []:
        return print("No se han introducido jugadores")
    while numero > len(lista_jugadores):
        numero = numero - len(lista_jugadores)
    return print(lista_jugadores[numero-1])



duck_duck_goose(sys.argv)